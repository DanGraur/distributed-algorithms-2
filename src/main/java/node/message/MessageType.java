package main.java.node.message;

/**
 * @author Dan Graur 11/25/2017
 */
public enum MessageType {
    REGULAR,
    /**
     * Message sent when state recording is performed
     */
    MARKER
}
